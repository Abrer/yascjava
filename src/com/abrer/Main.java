package com.abrer;

import com.sun.deploy.util.ArrayUtil;

import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
    }

    public static int num_of_networks(String net_class, int net_bits){
        //
        int networks;
        int class_bits = 0;

        if (net_class.equalsIgnoreCase("A")){
            class_bits = 8;
        }   else if (net_class.equalsIgnoreCase("B")){
            class_bits = 16;
        }   else if (net_class.equalsIgnoreCase("C")){
            class_bits = 24;
        }

        networks = (int)Math.pow(2, net_bits - class_bits);

        // if decimals found in anwser, investigate -- python funct
        // would define networks to 'Classess' in event of decimal in 'networks'.

        return networks;
    }

    public static int num_of_hosts(int net_bits){
        int MAX_BITS = 32;
        int hosts_per_network = (int) (Math.pow(2, MAX_BITS - net_bits) - 2);

        return hosts_per_network;
    }

    public static String convert_address_to_binary(String tmp_address){

        String[] address = split_octets(tmp_address);
        String binary_address = "";

        for (String octet: address) {
            String binary_octet = Integer.toBinaryString(Integer.parseInt(octet));

            while (binary_octet.length() != 8){
                binary_octet = "0" + binary_octet;
            }

            binary_address += binary_octet + ".";
        }

        binary_address = binary_address.substring(0, binary_address.length() - 1);
        return binary_address;
    }

    public static String[] split_octets(String address){
        return address.split("\\.");
    }

    public static String get_class(String ip_address){
        String net_class = ""; // Placeholder value.
        String[] split_ip_address = split_octets(ip_address);

        int first_octet = Integer.parseInt(split_ip_address[0]);


        // Clean this up once the program is working.
        if (first_octet >= 0){
            net_class = "A";
        }
        else if (first_octet >= 128){
            net_class = "B";
        }
        else if (first_octet >= 192){
            net_class = "C";
        }
        else if (first_octet >= 224){
            net_class = "MULTICAST";
        }
        else if (first_octet >= 240) {
            net_class = "E / EXPERIMENTAL";
        }
        else if (first_octet == 127){
            net_class = "LOOPBACK";
        }
        else{
            net_class = "CLASSLESS";
        }

        return net_class;
    }

    public static int get_working_octet(String subnet_mask){
        // Return the octet in which we can work
        int working_octet = 0;

        String[] address = split_octets(subnet_mask);

        for (int i = 0; i < 4; i++){
            // if address[i] NOT EQUAL TO "255"
           if (!address[i].equals("255")){
               working_octet = i + 1;
               break;
           } else if (i == 3){
               working_octet = Integer.parseInt(address[i]);
           }


        }
        return working_octet;  // Placeholder return statement
       }

    public static String cidr_to_decimal(String string_network_bits){
        // Convert CIDR notation to decimal equiv.. EX: 255.255.255.0
        string_network_bits = string_network_bits.replaceFirst("/", ""); // Get rid of "/" character
        int network_bits = Integer.parseInt(string_network_bits);   // convert bits to an integer

        // Some handy vars to help figure all this out...
        int full_octets = network_bits / 8;
        int octets_left = 4 - full_octets;
        int remaining_bits = network_bits % 8;

        String decimal_mask = "";


        // The equivalent of a Python Dictionary!
        HashMap<Integer, String> bit_decimal_vals = new HashMap<Integer, String>();

        // Map network bit to decimal place, to be used further down.
        bit_decimal_vals.put(0, "0.");
        bit_decimal_vals.put(1, "128.");
        bit_decimal_vals.put(2, "192.");
        bit_decimal_vals.put(3, "224.");
        bit_decimal_vals.put(4, "240.");
        bit_decimal_vals.put(5, "248.");
        bit_decimal_vals.put(6, "252.");
        bit_decimal_vals.put(7, "254.");
        bit_decimal_vals.put(8, "255.");

        // Determine decimal value in the relevant octet.
        if (full_octets == 0) {
            decimal_mask += bit_decimal_vals.get(remaining_bits);
        } else {
            for (int i = 0; i < full_octets ; i++) {
                decimal_mask += "255.";
                if (i == full_octets - 1){
                    if (remaining_bits > 0){
                        decimal_mask += bit_decimal_vals.get(remaining_bits);
                    }
                }
            }
        }

        // Add 0's to remaining octets.
        for (int i = 0; i < octets_left; i++) { // octets left - 1 was original comparison
            if (octets_left == 0) {
                break;
            }
            else {
                decimal_mask += "0.";
            }
        }

        //////////////////////////////////////////
        //  TODO
        // if len > 4, remove last item in list.
        // decimal_mask = decimal_mask.split('.')
        // for item in decimal mask, if len decimal mask > 4, dec-mask.pop(4)

        // new_mask = ""
        // for item in decimal mask:
        //      new_mask += val + "."

        // return new_mask[:-1] (remove dot at end)
        // ^^^^^ PORT ABOVE TO JAVA
        //
        //  For now, method is functioning as needed. Revisit me!
        //
        ////////////////////////////////////////////

        String[] temp_mask = split_octets(decimal_mask);
       // String[] new_mask;

        // One day when I revisit this method's logic, this won't be needed.
        // If there are more than 4 octets, remove the extra.
        if (temp_mask.length > 4){
            temp_mask = Arrays.copyOf(temp_mask, temp_mask.length - 1);
        }

        // Add periods to new mask
//        for (String octet: temp_mask) {
//            octet = octet + ".";
//        }

        //Remove period at end of the string. FUCK YA! REGEX!
        //decimal_mask = decimal_mask.replaceAll("\\.$", "");
        String new_mask_string = "";

        for (String element: temp_mask) {
            new_mask_string += element + ".";
        }

        new_mask_string = new_mask_string.replaceAll("\\.$", "");

        return new_mask_string;
    }

    public static String get_first_usable_address(String network_address){
        // Finish me :)

        //String to hold finished result
        String first_usable = "";
        String[] split_address = split_octets(network_address);

        //Temporary var for adding + 1 to last octet.
        int temp = 0;

        //Apparently, first usable addr = last octet + 1
        for (int i = 0; i < split_address.length; i++) {
            if (i == 3){
                temp = Integer.parseInt(split_address[i]); // Here is where we +1 last octet
                temp++;                                    // and here.
                split_address[i] = Integer.toString(temp);  // Now convert last octet back to string.
            }

            first_usable += split_address[i] + "."; // str = all octets separated by a period.
        }

        first_usable = first_usable.replaceAll("\\.$", ""); // Remove . at end of last octet.

        return first_usable;
    }

    public static String get_last_usable_address(String broadcast_address){
        //
        //String to hold finished result
        String last_usable = "";
        String[] split_address = split_octets(broadcast_address);

        //Temporary var for subtracting 1 from last octet.
        int temp = 0;

        //Apparently, first usable addr = last octet - 1
        for (int i = 0; i < split_address.length; i++) {
            if (i == 3){
                temp = Integer.parseInt(split_address[i]); // Here is where we +1 last octet
                temp--;                                    // and here.
                split_address[i] = Integer.toString(temp);  // Now convert last octet back to string.
            }

            last_usable += split_address[i] + "."; // str = all octets separated by a period.
        }

        last_usable = last_usable.replaceAll("\\.$", ""); // Remove . at end of last octet.

        return last_usable;
    }

    public static String get_broadcast_address(String ip_net_address, int working_octet, int net_increment, int net_bits){
        //
        // Finish me :)
        String[] octets = split_octets(ip_net_address);
        String broadcast = "";
        working_octet--;
        int temp = 0;

        if (net_bits == 24)     //  /24 is a special case here.
            octets[3] = "255";
//        else if (net_bits == 16){   //  /16 testing here
//            octets[2] = "255";
//        }
//        else if (net_bits == 8){    //  /8 testing here
//            octets[1] = "255";
//        }
        else{
            for (int i = 0; i < octets.length; i++) {
                if (i == working_octet){
                    temp = Integer.parseInt(octets[i]);
                    temp = temp + net_increment - 1;
                    octets[i] = Integer.toString(temp);

                }

                // Added more IF blocks here to try to curb the problem with /8 /16 /24
                if (net_bits == 8 || net_bits == 16){
                    if (i > working_octet - 1){
                        octets[i] = "255";
                    }
                }

                else if (i > working_octet) //TEST OFF BY ONE.
                    octets[i] = "255";
            }
        }
         //Add loop through process to concatenate the array to a string separated by periods, then chop end period off!
                //Return the result.
        for (int i = 0; i < octets.length; i++) {
            broadcast += octets[i] + ".";
        }

        broadcast = broadcast.replaceAll("\\.$", "");


        return broadcast;
    }

    public static String get_network_address(String ip_address, int working_octet, int net_increment, int net_bits){
        //
        // Finish me :)
        // Verify that I work!!!!

        working_octet -= 1;
        String[] octets = split_octets(ip_address);


        // For every octet past working octet, set to 0.
        for (int i = 0; i < octets.length; i++) {
            if (i > working_octet)
                octets[i] = "0";
        }

        // Again, /24 is a special case.
        if (net_bits == 24 || net_bits == 16 || net_bits == 8)
//        if (net_bits == 24)
            octets[working_octet] = "0";
        else {
            int temp = Integer.parseInt(octets[working_octet]);
            temp = (temp / net_increment) * net_increment;
            octets[working_octet] = Integer.toString(temp);
        }

        String network_address = "";

        for (String val: octets) {
            network_address += val + ".";
        }

        network_address = network_address.replaceAll("\\.$", ""); // replace end period.

        return network_address;
    }

    public static int get_network_bits(String subnet_mask){
        //
        // Finish me :)

        // The equivalent of a Python Dictionary!
        HashMap<String, Integer> bit_decimal_vals = new HashMap<String, Integer>();

        // Map network bit to decimal place, to be used further down.
        bit_decimal_vals.put("0", 0);
        bit_decimal_vals.put("128", 1);
        bit_decimal_vals.put("192", 2);
        bit_decimal_vals.put("224", 3);
        bit_decimal_vals.put("240", 4);
        bit_decimal_vals.put("248", 5);
        bit_decimal_vals.put("252", 6);
        bit_decimal_vals.put("254", 7);
        bit_decimal_vals.put("255", 8);

        String[] mask = split_octets(subnet_mask);

        int bits = 0;

        for (String octet: mask) {
            if (octet.equals("255"))
                bits += 8;

            if (!octet.equals("255")){
                int temp = bit_decimal_vals.get(octet);
                bits += temp;
            }
        }
        return bits;
    }

    public static int get_net_increment(int working_octet_value){
        //
        // Finish me :)

        int value = 0;

        if (working_octet_value == 0)
            value = 1;
        else{
            value = 256 - working_octet_value;
        }


        return value;
    }

    public static int get_working_octet_value(String subnet_mask){
        // Return the DECIMAL value of working octet

        String[] octets = split_octets(subnet_mask);
        int working_octet_value = 0;

        for (int i = 0; i < octets.length; i++) {
            if (!(Integer.parseInt(octets[i]) == 255)){
                working_octet_value = Integer.parseInt(octets[i]);
                break;
            } else if (i == 3){
                working_octet_value = Integer.parseInt(octets[i]);
            }
        }


        return working_octet_value;
    }


}

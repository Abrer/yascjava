package com.abrer;

import java.util.Arrays;

/**
 * Created by abrer on 6/25/16.
 */

public class Test extends Main {
    public static void main(String[] args){


        //11.22.33.44 /16 (/16 does not return 4 OCTETS!!!! INVESTIGATE!



        for (int i = 0; i < args.length; i++) {
            System.out.println(i + " is: " + args[i]);
        }

        //Some handy vars to display!
        String subnet_mask;
        String ip_address = args[0];

        //Detect CIDR notation.
        if (args[1].contains("/")) {
            subnet_mask = Main.cidr_to_decimal(args[1]);
        } else {
            subnet_mask = args[1];
        }

        // More handy vars!
        String net_class = Main.get_class(ip_address);
        int net_bits = Main.get_network_bits(subnet_mask);
        int wrk_octet = Main.get_working_octet(subnet_mask);
        int wrk_octet_val = Main.get_working_octet_value(subnet_mask);

        int net_increment = Main.get_net_increment(wrk_octet_val);
        String binary_mask = Main.convert_address_to_binary(subnet_mask);
        String net_address = Main.get_network_address(ip_address, wrk_octet, net_increment, net_bits);
        String broadcast_address = Main.get_broadcast_address(net_address, wrk_octet, net_increment, net_bits); //investigate w /8 /16 /24
        String last_usable_address = Main.get_last_usable_address(broadcast_address);                           // investigate w /8 /16 /24
        String first_usable_address = Main.get_first_usable_address(net_address);
        int available_hosts = Main.num_of_hosts(net_bits);
        int available_subnets = Main.num_of_networks(net_class, net_bits);

        if (ip_address.equalsIgnoreCase(net_address)){
            System.out.println("\n\tNETWORK ADDRESS!");
        } else if (ip_address.equalsIgnoreCase(broadcast_address)){
            System.out.println("BROADCAST ADDRESS!");
        }

        // Print our vars to the screen.
        System.out.println("");
        System.out.println("Ip Address: \t" + ip_address + " /" + net_bits);
        System.out.println("Subnet Mask: \t" + subnet_mask);
        System.out.println("Binary Mask: \t" + binary_mask);
        System.out.println("---------------");
        System.out.println("Class: \t\t" + net_class);
        System.out.println("Net Increment: \t" + net_increment);
        System.out.println("Network: \t" + net_address);
        System.out.println("Host Range: \t" + first_usable_address + " - " + last_usable_address);
        System.out.println("Broadcast: \t" + broadcast_address);
        System.out.println("---------------");
        System.out.println("Avail Hosts: \t" + available_hosts);
        System.out.println("Avail Subnets: \t" + available_subnets);
    }
}
